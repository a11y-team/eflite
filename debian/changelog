eflite (0.4.1-14) UNRELEASED; urgency=medium

  [ Samuel Thibault ]
  * control: Bump Standards-Version to 4.6.0 (no change)
  * patches/gcc-15: Fix build with gcc-15.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on flite1-dev.
  * Use secure URI in Homepage field.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 19 Sep 2021 23:50:15 -0000

eflite (0.4.1-13) unstable; urgency=medium

  * watch: update version to 4.
  * control: Set Rules-Requires-Root to no.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 18 Sep 2021 18:02:30 +0200

eflite (0.4.1-12) unstable; urgency=medium

  * Bump debhelper from 10 to 12.
  * patches/flags: Fix passing hardening CPPFLAGS.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 01 Jan 2021 16:47:53 +0100

eflite (0.4.1-11) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Update alioth list domain.
  * control: Fix libasound-dev build-dep to libasound2-dev.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 10.
  * Fix day-of-week for changelog entry 0.2.2-2.
  * Update standards version to 4.5.0, no changes needed.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 02 Aug 2020 01:25:29 +0200

eflite (0.4.1-10) unstable; urgency=medium

  * control: Set Vcs-* to salsa.debian.org.
  * control: Bump Standards-Version to 4.4.0 (no changes).
  * watch: Generalize pattern.
  * control: Fix homepage field.
  * control: Build-depend on libasound-dev to support alsa.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 01 Dec 2019 14:40:12 +0100

eflite (0.4.1-9) unstable; urgency=medium

  * control: Remove Mario from Uploaders (Closes: Bug#840869).
  * Use canonical anonscm vcs URL.
  * control: Update maintainer mailing list.
  * control: Migrate priority to optional.
  * control: Bump Standards-Version to 4.1.4.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 28 Apr 2018 17:18:18 +0200

eflite (0.4.1-8) unstable; urgency=medium

  * README.testing: Fix testing for eflite, not flite...

 -- Samuel Thibault <sthibault@debian.org>  Sun, 04 Sep 2016 18:36:33 +0200

eflite (0.4.1-7) unstable; urgency=low

  * watch: Generalize URL.
  * Bump Standards-Version to 3.9.8 (no changes).
  * compat: Bump to 9.
  * control: Drop hardening-wrapper dep (Closes: Bug#836623).
  * rules: Clear.
  * patches: switch to 3.0 patch system.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 04 Sep 2016 18:20:56 +0200

eflite (0.4.1-6) unstable; urgency=low

  * rules: Fix build against multi-arch libflite (Closes: Bug#676730).
  * control: build-depend on multi-arch libflite.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 10 Jun 2012 17:12:17 +0200

eflite (0.4.1-5) unstable; urgency=low

  [ Samuel Thibault ]
  * Set eflite Multi-Arch: foreign.
  * Bump Standards-Version to 3.9.3 (no change needed)
  * Depend on hardening-check.
  * Set DEB_BUILD_HARDENING=1 to enable hardening.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 08 Jun 2012 22:47:53 +0200

eflite (0.4.1-4) unstable; urgency=low

  [ Mario Lang ]
  * Add Homepage field to debian/control.

  [ Samuel Thibault ]
  * es.c: Fix buffer overflow (Closes: #507108).
  * Makefile.in: Fix flite link flags (Closes: #577864).
  * debian/control:
    - Add myself in Uploaders.
    - Bump Standards-Version to 3.9.1 (no changes needed).
    - Document that only English is supported.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 27 Jul 2010 01:48:07 +0200

eflite (0.4.1-3) unstable; urgency=low

  * Apply upstream patches from CVS to fix a race (Closes: Bug#542468).
  * Disable test during build since it requires sound hardware (fixes
    FTBFS).

 -- Mario Lang <mlang@debian.org>  Thu, 27 Aug 2009 15:17:11 +0200

eflite (0.4.1-2) unstable; urgency=low

  * Switch to debhelper 7.
  * Add Vcs-* fields to debian/control.
  * Fix debian/watch file to use sf.net redirector (which is broken right now)
    (Closes: Bug#449891).
  * Upgrade ancient Standards-Version to 3.8.3.
  * Add Copyright years/holders to debian/copyright.
  * Add device: description to eflite.blurb.

 -- Mario Lang <mlang@debian.org>  Thu, 27 Aug 2009 13:52:05 +0200

eflite (0.4.1-1) unstable; urgency=low

  * New upstream release (Closes: Bug#391511).

 -- Mario Lang <mlang@debian.org>  Fri, 26 Jan 2007 20:38:30 +0100

eflite (0.3.8-2) unstable; urgency=low

  * Fix blurb file (Closes Bug#354918).

 -- Mario Lang <mlang@debian.org>  Sun, 23 Apr 2006 12:36:58 +0200

eflite (0.3.8-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Mario Lang <mlang@debian.org>  Wed, 25 Feb 2004 13:55:10 +0100

eflite (0.3.7-2) unstable; urgency=low

  * Lower the Recommends: emacspeak to Suggests, and add yasr too.

 -- Mario Lang <mlang@debian.org>  Thu, 20 Nov 2003 17:31:19 +0100

eflite (0.3.7-1) unstable; urgency=low

  * New upstream version.

 -- Mario Lang <mlang@debian.org>  Fri,  1 Aug 2003 14:45:10 +0200

eflite (0.3.6-1) unstable; urgency=low

  * New upstream release (merged the flite_include_dir patch upstream)
  * Set Standards-Version to 3.5.9
  * Use dh_compat 4 and depend on debhepler >> 4.
  * Patch Makefile.in to link with shared libs.

 -- Mario Lang <mlang@debian.org>  Wed, 26 Mar 2003 10:33:51 +0100

eflite (0.3.5-2) unstable; urgency=low

  * Relink against 16bit voice (Closes: Bug#169803)

 -- Mario Lang <mlang@debian.org>  Thu, 13 Feb 2003 09:59:20 +0100

eflite (0.3.5-1) unstable; urgency=low

  * New upstream version (Closes: Bug#177294)

 -- Mario Lang <mlang@debian.org>  Tue, 21 Jan 2003 11:00:46 +0100

eflite (0.3.4-1) unstable; urgency=low

  * New upstream version.

 -- Mario Lang <mlang@debian.org>  Tue, 10 Dec 2002 11:20:02 +0100

eflite (0.3.3-1) unstable; urgency=low

  * New upstream version.
  * Add eflite.blurb file (Closes: Bug#169802)
  * Add "Recommends: emacspeak" to debian/control.

 -- Mario Lang <mlang@debian.org>  Wed, 20 Nov 2002 11:30:00 +0100

eflite (0.3.2-1) unstable; urgency=low

  * New upstream version.

 -- Mario Lang <mlang@delysid.org>  Tue, 25 Jun 2002 00:38:52 +0200

eflite (0.2.2-2) unstable; urgency=low

  * Make it lintian clean.

 -- Mario Lang <mlang@delysid.org>  Mon, 24 Jun 2002 20:38:52 +0200

eflite (0.2.2-1) unstable; urgency=low

  * Initial release.

 -- Mario Lang <mlang@delysid.org>  Tue,  7 May 2002 12:38:52 +0200
